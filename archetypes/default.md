---
title: '{{ replace .File.ContentBaseName "-" " " | title }}'
date: {{ .Date }}
draft: true
summary: "your summary"
coverIntro: "Your Cover Intro"
liveLink: "Your live link"
codeLink: "your code link"
techStack: ["your", "tech", "stack"]
---