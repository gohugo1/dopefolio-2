+++
title = 'Project 1'
date = 2023-01-15T09:00:00-07:00
draft = false
summary = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic facilis tempora, explicabo quae quod deserunt eius sapiente praesentium."
coverIntro = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos in numquam incidunt earum quaerat cum fuga, atque similique natus nobis sit."
cover = "/projects/project-1/project-mockup-example.jpeg"
liveLink = "https://www.google.com"
codeLink = "https://www.github.com"
techStack = [ 
    "HTML",
    "CSS",
    "JavaScript",
    "React",
    "SASS",
    "GIT",
    "Shopify",
    "Wordpress",
    "Google ADS",
    "Facebook Ads",
    "Android",
    "IOS",
]
+++

 Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque alias tenetur minus quaerat aliquid, aut provident blanditiis, deleniti aspernatur ipsam eaque veniam voluptatem corporis vitae mollitia laborum corrupti ullam rem. Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque alias tenetur minus quaerat aliquid, aut provident blanditiis, deleniti aspernatur ipsam eaque veniam voluptatem corporis vitae mollitia laborum corrupti ullam rem?

Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque alias tenetur minus quaerat aliquid, aut provident blanditiis, deleniti aspernatur ipsam eaque veniam voluptatem corporis vitae mollitia laborum corrupti ullam rem? 
