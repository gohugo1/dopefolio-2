---
title: About Me
date: 2023-01-15T16:00:00.000Z
draft: false
summary: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic facilis
  tempora explicabo quae quod deserunt eius sapiente "
skills:
  - HTML
  - CSS
  - JavaScript
  - React
  - SASS
  - GIT
  - Shopify
  - Wordpress
  - Google ADS
  - Facebook Ads
  - Android
  - IOS
layout: about
---


Hey! It's John Doe and I'm a Frontend Web Developer located in Los Angeles. I've done remote projects for agencies, consulted for startups, and collaborated with talented people to create digital products for both business and consumer use.

I'm a bit of a digital product junky. Over the years, I've used hundreds of web and mobile apps in different industries and verticals. Feel free to contact me here.
